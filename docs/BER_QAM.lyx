#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{lmodern}

\newcommand{\longeq}{=\joinrel=}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_title "BER of QAM"
\pdf_author "Yihua Tan"
\pdf_keywords "BER, QAM"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize a4paper
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 2cm
\topmargin 2cm
\rightmargin 2cm
\bottommargin 2cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Bit Error Rate (BER) of Quadrature Amplitude Modulation (QAM)
\end_layout

\begin_layout Author
Yihua Tan
\end_layout

\begin_layout Standard
Consider quadrature amplitude modulation (QAM) of size 
\begin_inset Formula $M=2^{b}=p^{2}$
\end_inset

.
 Suppose the maximum x axis or y axis is 
\begin_inset Formula $a$
\end_inset

.
 Then the average power will be 
\begin_inset Formula 
\begin{eqnarray}
P & = & \frac{1}{M}\cdot\sum_{j=0}^{p-1}\sum_{i=0}^{p-1}\left[\left(-a+\frac{2a}{p-1}\cdot i\right)^{2}+\left(-a+\frac{2a}{p-1}\cdot j\right)^{2}\right]\nonumber \\
 & = & \frac{1}{M}\cdot\sum_{j=0}^{p-1}\sum_{i=0}^{p-1}\left(-a+\frac{2a}{p-1}\cdot i\right)^{2}+\frac{1}{M}\cdot\sum_{j=0}^{p-1}\sum_{i=0}^{p-1}\left(-a+\frac{2a}{p-1}\cdot j\right)^{2}\nonumber \\
 & = & \frac{p}{M}\cdot\left[\sum_{i=0}^{p-1}\left(-a+\frac{2a}{p-1}\cdot i\right)^{2}+\sum_{j=0}^{p-1}\left(-a+\frac{2a}{p-1}\cdot j\right)^{2}\right]\nonumber \\
 & = & \frac{2}{p}\cdot\sum_{i=0}^{p-1}\left(-a+\frac{2a}{p-1}\cdot i\right)^{2}\nonumber \\
 & = & \frac{2}{p}\cdot a^{2}\cdot\sum_{i=0}^{p-1}\left(-1+\frac{2}{p-1}\cdot i\right)^{2}\nonumber \\
 & = & \frac{2}{p}\cdot a^{2}\cdot\sum_{i=0}^{p-1}\left(1-\frac{4}{p-1}\cdot i+\frac{4}{\left(p-1\right)^{2}}\cdot i^{2}\right)\nonumber \\
 & = & \frac{2}{3}\cdot a^{2}\cdot\frac{p+1}{p-1}
\end{eqnarray}

\end_inset

where the last equality is obtained using 
\begin_inset Formula $\sum_{i=1}^{n}i^{2}=\frac{n\left(n+1\right)\left(2n+1\right)}{6}$
\end_inset

.
 Normalize the signal power to 
\begin_inset Formula $P=1$
\end_inset

, then we have 
\begin_inset Formula 
\begin{equation}
a=\sqrt{\frac{3\left(p-1\right)}{2\left(p+1\right)}}.
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Denote the signal-to-noise ratio by 
\begin_inset Formula $\gamma=\frac{P}{N_{0}}$
\end_inset

, where 
\begin_inset Formula $N_{0}=2\sigma^{2}$
\end_inset

.
 Denote the noise by 
\begin_inset Formula $w=w_{I}+j\cdot w_{Q}$
\end_inset

 with 
\begin_inset Formula $w_{I}\sim N\left(0,\sigma^{2}\right)$
\end_inset

 and 
\begin_inset Formula $w_{Q}\sim N\left(0,\sigma^{2}\right)$
\end_inset

.
 The separation between two neighbor constellation points is 
\begin_inset Formula 
\[
\frac{2a}{p-1}=2\sqrt{\frac{3\left(p-1\right)}{2\left(p+1\right)}}\cdot\frac{1}{p-1}=\sqrt{\frac{6}{M-1}}.
\]

\end_inset

If the real part of image part of the noise in a symbol is larger than half
 of the separation, then the minimum-distance demodulator will mistake the
 constellation point for a neighbor constellation point.
 So we define a distance threshold 
\begin_inset Formula 
\begin{equation}
d=\frac{1}{2}\textrm{separation}=\frac{1}{2}\cdot\sqrt{\frac{6}{M-1}}=\sqrt{\frac{3}{2\left(M-1\right)}}.
\end{equation}

\end_inset

Then the probability of mistaking the constellation point for a neighbor
 constellation point in the positive real direction is 
\begin_inset Formula 
\begin{equation}
p_{1}=\int_{d}^{\infty}f\left(w_{I}\right)dw_{I}=\int_{\frac{d}{\sigma}}^{\infty}\frac{1}{\sqrt{2\pi}}e^{-\frac{u^{2}}{2}}du=Q\left(\frac{d}{\sigma}\right)=Q\left(\sqrt{\frac{3\gamma}{M-1}}\right).
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Among the 
\begin_inset Formula $M$
\end_inset

 constellation points, there are 
\begin_inset Formula $4$
\end_inset

 points that have only 
\begin_inset Formula $2$
\end_inset

 neighbor points, 
\begin_inset Formula $4\left(\sqrt{M}-2\right)$
\end_inset

 points that have 
\begin_inset Formula $3$
\end_inset

 neighbor points, and 
\begin_inset Formula $\left(\sqrt{M}-2\right)^{2}$
\end_inset

 points that have 
\begin_inset Formula $4$
\end_inset

 neighbor points.
 Therefore, the symbol error rate (SER) is 
\begin_inset Formula 
\begin{eqnarray}
SER & = & \frac{1}{M}\left\{ 4\left[1-\left(1-p_{1}\right)^{2}\right]+4\left(\sqrt{M}-2\right)\left[1-\left(1-2p_{1}\right)\left(1-p_{1}\right)\right]+\left(\sqrt{M}-2\right)^{2}\left[1-\left(1-2p_{1}\right)^{2}\right]\right\} \nonumber \\
 & = & \frac{4p_{1}}{M}\left[M-\sqrt{M}-p_{1}\left(\sqrt{M}-1\right)^{2}\right].
\end{eqnarray}

\end_inset

At high SNR, the SER could be approximated by 
\begin_inset Formula $4p_{1}\left(1-\frac{1}{\sqrt{M}}\right)$
\end_inset

.
 Note that when 
\begin_inset Formula $M$
\end_inset

 is large, then 
\begin_inset Formula $\gamma$
\end_inset

 needs to be even larger to make the term 
\begin_inset Formula $p_{1}\left(\sqrt{M}-1\right)^{2}$
\end_inset

 negligible.
 If SNR is efficiently high and 
\begin_inset Formula $M$
\end_inset

 is efficiently large, then the SER can be further approximated by 
\begin_inset Formula $4p_{1}$
\end_inset

.
 
\end_layout

\begin_layout Standard
If we use gray mapping, then the BER will be 
\begin_inset Formula 
\begin{equation}
BER=\frac{SER}{b}.
\end{equation}

\end_inset

Note that if we do not apply gray mapping, the BER can be several times
 larger than that of using gray mapping.
 
\end_layout

\end_body
\end_document
