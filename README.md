# OFDM simulation

**Author**: *Yihua Tan* (adherentx.tyh@gmail.com)

**Affiliation**: Department of Information Engineering, The Chinese University of Hong Kong, Shatin, N.T., Hong Kong

## Introduction
This program simulates the TX and RX of orthogonal frequency-division multiplexing (OFDM).
It is compatible with Python 2 and Python 3.

It is not complete yet. But it should be good enough to capture the essence of OFDM communications and serve as
a tutorial of OFDM.
