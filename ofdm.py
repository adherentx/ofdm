#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
OFDM simulation.
"""

from __future__ import division

import numpy as np
import scipy as sp
import scipy.special
import matplotlib.pyplot as plt
import gray_qammod

__author__ = "Yihua Tan"
__copyright__ = "Copyright (C) 2016 Yihua Tan"
__version__ = "0.0.5"
__email__ = "ty013@ie.cuhk.edu.hk"


""" abbreviations used in this file
ofdm: orthogonal frequency-division multiplexing
sc: subcarrier
cp: cyclic prefix
lts: long training sequence
sts: short training sequence
qam: quadrature amplitude modulation
"""


""" simulation config """
simplified_channel = True

test_perfect_chan_est = False
test_perfect_cfo_est = False
test_disable_pilot = False
if test_perfect_chan_est or test_perfect_cfo_est or test_disable_pilot:
    print("warning: testing")


""" global parameters
parameters that both the transmitter and the receiver know
"""
N = 64  # FFT size
cp_len = 16  # the length of the cyclic prefix (CP)
sym_len = N + cp_len
data_sc_num = 48  # the number of data subcarriers

pilot_sc_indices = np.array([7, 21, 43, 57], dtype=int)  # pilot subcarrier indices
pilot_sc_values = np.array([1, 1, 1, -1], dtype=complex)

data_sc_indices = np.array(list(range(1, 7)) + list(range(8, 21)) + list(range(22, 27))
                           + list(range(38, 43)) + list(range(44, 57)) + list(range(58, 64)), dtype=int)
assert(data_sc_indices.size == data_sc_num)

lts_f = np.array(
    [0, 1, -1, -1, 1, 1, -1, 1, -1, 1, -1, -1, -1, -1, -1, 1,
     1, -1, -1, 1, -1, 1, -1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 1, 1, -1, -1, 1, 1, -1, 1, -1, 1,
     1, 1, 1, 1, 1, -1, -1, 1, 1, -1, 1, -1, 1, 1, 1, 1], dtype=complex)  # freq-domain LTS
assert(lts_f.size == N)
lts_t = np.fft.ifft(lts_f, N)  # time-domain LTS

used_indices = np.where(lts_f != 0)
shifted_indices = np.array(list(range(0, 32)) + list(range(-32, 0)), dtype=int)  # shifted FFT indices

qam_size = 1024
assert(np.mod(np.log2(qam_size), 1) < 1e-10)  # integer bits
qam_bits = int(np.round(np.log2(qam_size)))
assert(np.mod(np.sqrt(qam_size), 1) == 0)  # the constellation is restricted to MxM presently
qam_max_axis = np.sqrt(qam_size) - 1
qam_mod = gray_qammod.GrayQamMod(qam_size)


""" channel model parameters """
snr_db = 30
snr = 10 ** (snr_db/10)
bandwidth = 20e6  # unit: Hz
cfo_hz = 5000  # unit: Hz
cfo = cfo_hz / bandwidth  # rotation/sample


"""------------------------ TX ------------------------"""

""" message generation """
message_len = 100000  # typical 802.11 packets are about 1k bytes (8k bits)
message = np.random.randint(2, size=message_len)

""" optional: scrambling and interleaving """
# TODO

""" channel encoding """
# TODO
encoded_len = message_len
encoded = message

""" modulation """
modulated = qam_mod.modulate(encoded)  # bits padding is automatic
modulated /= qam_max_axis  # normalization
modulated_len = modulated.size

""" frequency domain: fill in data and pilots """
ofdm_sym_num = int(np.ceil(modulated_len / data_sc_num))
all_sc = np.zeros((N, ofdm_sym_num), dtype=complex)
all_sc[data_sc_indices, :] = np.reshape(
    np.concatenate((modulated, np.zeros(data_sc_num*ofdm_sym_num-modulated_len, dtype=complex))),
    (data_sc_num, ofdm_sym_num),
    order='F')
all_sc[pilot_sc_indices, :] = np.reshape(pilot_sc_values, (pilot_sc_values.size, 1))

""" IFFT: freq domain to time domain """
time_symbols = np.fft.ifft(all_sc, n=N, axis=0)

""" cyclic prefix (CP) """
time_symbols_cp = np.concatenate((time_symbols[-cp_len:, :], time_symbols), axis=0)
ofdm_symbols = np.reshape(time_symbols_cp, (sym_len*ofdm_sym_num, ), order='F')  # serialization

""" packet format: preamble + OFDM symbols
omit STS, only use two LTS as the preamble
"""
preamble = np.concatenate((lts_t[-cp_len*2:], lts_t, lts_t))
tx_packet = np.concatenate((preamble, ofdm_symbols))


"""------------------------ Channel ------------------------"""

""" channel impulse response """
if simplified_channel:
    """ simplified channel model that only has multi-path and CFO. """
    channel_response = np.array([0.6+0.8j, 0.1-0.1j, 0.05+0.05j, 0.003, 0.0004j], dtype=complex)
    filtered_packet = np.convolve(tx_packet, channel_response)

    """ calculate RX signal power """
    # Please note that after the FFT, the power of time-domain signal is 1/N that of the freq-domain signal.
    # rx_signal_pow ~ 0.01
    rx_signal_pow = np.linalg.norm(filtered_packet)**2 / filtered_packet.size
    # print("RX signal power: {}".format(rx_signal_pow))

    """ carrier frequency offset (CFO) and sampling frequency offset (SFO)
    TODO: take into account the SFO later
    """
    chan_packet = filtered_packet * np.exp(1j * 2 * np.pi * cfo * np.array(list(range(0, filtered_packet.size)), dtype=complex))
else:
    """ more realistic channel that has multi-path, pulse shaping, up/down converts, CFO/SFO, and doppler spread. """
    raise NotImplementedError

# The generation of noise below assumes the knowledge of the RX power, such that the SNR can be more accurate.
# NOTE: The signal power is distributed on the 52 data subcarriers, while the noise power is distributed on 64
# subcarriers, making the SNR higher than specified. To compensate for it, we multiply the noise power by 64/52 below.
noise_sigma = np.sqrt(rx_signal_pow / snr / 2 * 64 / 52)  # N0=2sigma^2
noise = np.random.normal(0, noise_sigma, size=chan_packet.shape) \
        + 1j * np.random.normal(0, noise_sigma, size=chan_packet.shape)
if test_perfect_chan_est:
    noise[0:sym_len * 2] = 0  # NOTE: simulate perfect channel estimation

rx_packet = chan_packet + noise

# lts_conv = np.absolute(np.convolve(rx_packet, lts_t))
# plt.figure(1)
# plt.plot(lts_conv)
# plt.show()


"""------------------------ RX ------------------------"""

""" LTS sync """
# Tentatively, assume that packet synchronization is achieved, and the first sample of rx_packet corresponds to
# the first sample of tx_packet. This assumption removes the need to find the peaks of LTS correlation.
# Things will not grow much harder even if without this assumption.

rx_preamble = rx_packet[0:2*sym_len]
cp_cut_offset = -6

""" CFO estimation """
lts_corr = np.dot(np.conjugate(rx_preamble[2*cp_len+cp_cut_offset:2*cp_len+N+cp_cut_offset]),
                  rx_preamble[2*cp_len+N+cp_cut_offset:2*cp_len+2*N+cp_cut_offset])
cfo_est = np.angle(lts_corr) / (2*np.pi) / N  # unit: rotation/sample
if test_perfect_cfo_est:
    cfo_est = cfo  # NOTE: simulate perfect CFO compensation

""" CFO compensation """
rx_packet_cfo = rx_packet * np.exp(-1j * 2*np.pi * cfo_est * np.array(list(range(0, rx_packet.size)), dtype=complex))

""" CP cut """
rx_preamble = rx_packet_cfo[0:2*sym_len]
rx_lts_1 = rx_preamble[2*cp_len+cp_cut_offset:2*cp_len+N+cp_cut_offset]
rx_lts_2 = rx_preamble[2*cp_len+N+cp_cut_offset:2*cp_len+2*N+cp_cut_offset]
rx_data = rx_packet_cfo[2*sym_len:(2+ofdm_sym_num)*sym_len]
rx_symbols = np.reshape(rx_data, (sym_len, ofdm_sym_num), order='F')
rx_symbols_cut = rx_symbols[cp_len+cp_cut_offset:sym_len+cp_cut_offset, :]

""" FFT """
rx_symbols_freq = np.fft.fft(rx_symbols_cut, n=N, axis=0)
lts_avg = np.divide(rx_lts_1 + rx_lts_2, 2)  # using np.divide rather than /2, to suppress a warning at the line below
lts_avg_f = np.fft.fft(lts_avg, n=N)

""" channel estimation """
# FIXME: the channel estimation currently used here is naive.
freq_channel = np.zeros(N, dtype=complex)
freq_channel[used_indices] = lts_avg_f[used_indices] / lts_f[used_indices]
freq_channel = np.reshape(freq_channel, (N, 1))

""" channel compensation using initial channel estimates """
rx_symbols_chan_comp = np.zeros(rx_symbols_freq.shape, dtype=complex)
rx_symbols_chan_comp[used_indices, :] = rx_symbols_freq[used_indices, :] / freq_channel[used_indices]

""" channel compensation using pilots
TODO: the pilots should also be used to keep track of the amplitude variation, although the channel amplitude does
not change in the simulation presently.
"""
rx_pilots = rx_symbols_chan_comp[pilot_sc_indices, :]
rx_pilots_phase = np.angle(rx_pilots / np.reshape(pilot_sc_values, (pilot_sc_values.size, 1)))
# unwrap, TODO: robustness against SFO sample drift can be further improved
assert(pilot_sc_indices.size == 4)
rx_pilots_phase_shifted = np.concatenate((rx_pilots_phase[2:4, :],
                                          rx_pilots_phase[0:2, :]), axis=0)
rx_pilots_phase_shifted_unwrapped = np.unwrap(rx_pilots_phase_shifted, axis=0)
rx_pilots_phase_unwrapped = np.concatenate((rx_pilots_phase_shifted_unwrapped[2:4, :],
                                            rx_pilots_phase_shifted_unwrapped[0:2, :]), axis=0)
sc_phases_poly_coeff = np.polyfit(shifted_indices[pilot_sc_indices], rx_pilots_phase_unwrapped, 1)  # linear regression
sc_poly_mat = np.concatenate((np.reshape(shifted_indices, (N, 1)), np.ones((N, 1), dtype=int)), axis=1)
sc_phases_reg = np.matmul(sc_poly_mat, sc_phases_poly_coeff)
rx_symbols_pilot_comp = np.zeros(rx_symbols_chan_comp.shape, dtype=complex)
rx_symbols_pilot_comp[used_indices, :] = rx_symbols_chan_comp[used_indices, :] \
                                         / np.exp(1j * sc_phases_reg[used_indices, :])
if test_disable_pilot:
    rx_symbols_pilot_comp[used_indices, :] = rx_symbols_chan_comp[used_indices, :]  # TODO: simulate disabling pilot


""" serialization, data extraction """
rx_freq_data = np.reshape(rx_symbols_pilot_comp[data_sc_indices, :], (data_sc_num*ofdm_sym_num, ),
                          order='F')[0:modulated_len]

""" demodulation """
rx_freq_data_scaled = rx_freq_data * qam_max_axis  # scaling before demodulation
demodulated = qam_mod.demodulate(rx_freq_data_scaled)  # hard decision

""" channel decoding """
# TODO
decoded = demodulated

""" optional: deinterleaving and descrambling """
# TODO
message_decoded = decoded[0:message_len]


"""------------------------ statistics ------------------------"""

""" Please refer to docs/BER_QAM.pdf for the derivation of the BER calculation. """


def p1(m, gamma):
    # gamma: SNR, or energy per symbol / N0
    return 0.5 * sp.special.erfc(np.sqrt(3 * gamma / (m - 1)) / np.sqrt(2))


def find_ser(m, gamma):
    return 4 * p1(m, gamma) / m * (m - np.sqrt(m) - p1(m, gamma) * (np.sqrt(m) - 1) ** 2)


def find_ber(m, gamma):
    return find_ser(m, gamma) / np.log2(m)

""" expected BER, by the modulation and the SNR """
ber_expected = find_ber(qam_size, snr)

error_bit_num = np.sum(message != message_decoded)
ber = error_bit_num / message_len
print("BER: {}, expected: {}".format(ber, ber_expected))

# plot RX constellation
plt.figure()
plt.scatter(np.real(rx_freq_data), np.imag(rx_freq_data), s=10)
plot_lim = 1 + 10/snr_db
plt.xlim([-plot_lim, plot_lim])
plt.ylim([-plot_lim, plot_lim])
plt.xlabel("I")
plt.ylabel("Q")
plt.title("{}-QAM constellation".format(qam_size))
plt.show()
